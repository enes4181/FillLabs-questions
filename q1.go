package main

import (
	"sort"
	"strings"
)

type Word struct {
	Word  string
	Count int
}

type ByCount []Word

func (w ByCount) Len() int {
	return len(w)
}

func (w ByCount) Swap(i, j int) {
	w[i], w[j] = w[j], w[i]
}

func (w ByCount) Less(i, j int) bool {
	if w[i].Count == w[j].Count {
		return len(w[i].Word) > len(w[j].Word)
	}
	return w[i].Count > w[j].Count
}

func sortWords(words []string) {
	var w []Word
	for _, word := range words {
		count := strings.Count(word, "a")
		w = append(w, Word{word, count})
	}
	sort.Sort(ByCount(w))
	for _, v := range w {
		println(v.Word)
	}
}

func main() {
	words := []string{"aaaasd", "a", "aab", "aaabcd", "ef", "cssssssd", "fdz", "kf", "zc", "lklklklklklklklkl", "l"}
	sortWords(words)
}

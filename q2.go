package main

func recursiveFunc(num int) {
	if num <= 1 {
		return
	}

	recursiveFunc(num / 2)

	println(num)

}

func main() {
	recursiveFunc(9)

}

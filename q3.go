package main

import (
	"fmt"
)

func findMostRepeated(arr []string) string {
	wordMap := make(map[string]int)
	mostRepeatedWord := ""
	maxCount := 0

	for _, word := range arr {
		wordMap[word]++
		if wordMap[word] > maxCount {
			maxCount = wordMap[word]
			mostRepeatedWord = word
		}
	}

	return mostRepeatedWord
}

func main() {
	arr := []string{"apple", "pie", "apple", "red", "red", "red"}
	fmt.Println(findMostRepeated(arr))
}
